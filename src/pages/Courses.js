import coursesData from '../data/courses.js'
import CourseCard from '../components/CourseCard.js'


export default function Courses(){
//Check connection between mock data and Courses.js
			console.log(coursesData);
			console.log(coursesData[0]);

// The "course" in the CourseCard component is called a "prop" which is a shorthand for "property" since components are considered as objects in React JS
		    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ("")
		    // We can pass information from one component to another using props. This is referred to as "props drilling"

const courses = coursesData.map(course => {
	return(
		<>
		<CourseCard key={coursesData.id} courseProps={course}/>
		</>
	)
})
	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}
