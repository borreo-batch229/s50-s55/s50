import './App.css'
// AppNavbar Importation
import AppNavbar from './components/AppNavbar.js'
import Home from './pages/Home.js'
import {Container} from 'react-bootstrap'
import Courses from './pages/Courses.js'

function App(){
  return(
    // Fragment "<> and </>"
    /*We are mounting our components and to prepare for output rendering*/
    <>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>
  )
}
export default App;