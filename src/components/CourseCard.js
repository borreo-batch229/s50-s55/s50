import {Card, Button} from 'react-bootstrap';
import {useState} from 'react'

export default function CourseCard({courseProps}){
	console.log(courseProps);
	console.log(typeof courseProps);

	// Destructuring data to avoid dot notation
const {name, description, price} = courseProps;

// 3 Hooks in React
// 1. useState
// 2. useEffect
// 3. useContext

//Use useState hook for the component to be able to store state
// States are used to keep track of information related to individual components
//Syntax -> const [getter, setter] = useState(initialGetterValue);

const[count, setCount] = useState(0);
console.log(useState(0));
	

function enroll({

	setCount(count+1);
	console.log("Enrollees" + count)
})



	return(
	
          <Card className="my-3">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>Enrollees:</Card.Text>
	            <Button variant="primary">Enroll</Button>
	        </Card.Body>
	    </Card>
	
	)
}